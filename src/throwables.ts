import { httpcodes } from "./httpcodes";
export abstract class httpThrowable extends Error{
    public readonly httpcode:number;
    constructor(message:string,httpcode:number,debug:boolean = false){
        super(message);
        this.httpcode = httpcode;
        if(!debug){
            this.stack = "";
        }
    }
}

export class MissingParameterException extends httpThrowable{
    constructor(message:string,httpcode:number=httpcodes.BAD_REQUEST,debug:boolean = false){
        super(message,httpcode,debug);
        this.name = "Missing Parameter";
    }
}

export class AuthorizationFailed extends httpThrowable{
    constructor(message:string,httpcode:number=httpcodes.FORBIDDEN ,debug:boolean = false){
        super(message,httpcode,debug);
        this.name = "Authorization Failed";
    }
}