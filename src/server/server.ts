import { baseRouter,loginRouter } from "./router";
import express from "express";
import { c404, CerrorHandler } from "./controllers/errorsHandler";
import { verifyToken } from "./controllers/middlewares";

export default class Server{
    private readonly app;
    private readonly port:number;
    private readonly router;
    private readonly loginrouter;
    private readonly serveStatic;
    private readonly staticRoute;
    private readonly errorHandler;
    private readonly verifyToken;
    private alreadyListen = false;

    constructor({serveStatic=false, staticRoute="static"}:{serveStatic?:boolean,staticRoute?:string}){
        this.app = express();
        this.port = Number(process.env.PORT);
        this.router = baseRouter;
        this.loginrouter = loginRouter;
        this.serveStatic = serveStatic;
        this.staticRoute = staticRoute;
        this.errorHandler = CerrorHandler;
        this.verifyToken = verifyToken;
        this.init()
    }
    private init(){
        this.app.use(express.json());
        this.app.use(express.urlencoded({extended:false}));
        this.app.use(this.loginrouter);
        //!OJO, esto de aca podria tener alguna vulnerabilidad si un usuario llega a subir un archivo
        //!con mismo nombre que una ruta podria dejar inutilizada dicha ruta, pero lo debo dejar ahi
        //!ya que si no, no se podria acceder al archivo estatico sin token
        if(this.serveStatic){
            this.app.use("/",express.static(this.staticRoute));
        }
        this.app.use(this.verifyToken);
        this.app.use(this.router);
        this.app.use(c404);
        this.app.use(this.errorHandler);
    }
    public listen(){
        if(!this.alreadyListen){
            this.app.listen(this.port,()=>{
                this.alreadyListen = true;
                console.log(`Server Running on PORT ${this.port}`)
                return this;
            })
        }else{
            throw new Error("Server Already listening");
        }
    }
}