import type { Request, Response, NextFunction } from "express";
import { MissingParameterException } from "../../throwables";
import { JSONresponse } from "../../interfaces";

export function loginExample(req:Request,res:Response, _next:NextFunction){
    let {username,password} = req.body as {username:string,password:string};
    if(!username){
        throw new MissingParameterException("Missing parameter username");
        return;
    }
    if(!password){
        throw new MissingParameterException("Missing parameter password");
        return;
    }
    res.json( {response: {username,password} , done:true} as JSONresponse)
}