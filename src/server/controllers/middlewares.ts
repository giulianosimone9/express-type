import type { Response, NextFunction } from "express";
import { AuthorizationFailed } from "../../throwables";
import type { RequestUser } from "../../interfaces";

export function verifyToken(req:RequestUser,_res:Response,next:NextFunction){
    let token = req.headers['authorization'];
    if(!token && token !== "Bearer test"){
        throw new AuthorizationFailed("Not valid Token");
    }
    req.user = { id:"1" };
    next();
}