import type { Request, Response } from "express";
import type { JSONresponse } from "../../interfaces";
import { httpThrowable } from "../../throwables";
import type { ErrorRequestHandler } from "express";

export let CerrorHandler: ErrorRequestHandler = (err, _req, res, _next) => {   
    console.error(err);
    if(err instanceof httpThrowable){
        res.status(err.httpcode).json( {response:err.message, done:false} as JSONresponse);
    }else{
        res.status(500).json({response:"Internal server error", done: false} as JSONresponse);
    }
    return;
}
export function c404(_req:Request, res:Response){
    res.status(404).json({response:"Not found",done:false} as JSONresponse);
}