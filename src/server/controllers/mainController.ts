import type { Response } from "express";
import type { RequestUser } from "../../interfaces";

export function helloWorld(req:RequestUser, res:Response){
    console.log(req.user);
    res.send("Hello World");
}