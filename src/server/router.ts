import { Router } from "express"
import * as Controller from './controllers/mainController';
import * as LoginController from './controllers/loginController';
//Router para las rutas que no necesiten un token para ser usadas
const loginRouter = Router();
    //definimos las routas usando el controlador;  
    
    loginRouter.post("/login", LoginController.loginExample);
    
//Router para las rutas que necesiten un token para ser usadas
const baseRouter = Router();

    //definimos las routas usando el controlador;  
    baseRouter.all("/",Controller.helloWorld);
export {baseRouter, loginRouter};