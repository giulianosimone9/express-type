import type { Request } from "express";
export interface JSONresponse{
    response: String | Object,
    done:Boolean,
}

export interface RequestUser extends Request{
    user?:{
        id:string
    }
}